# Tretton37

# Short Description

This project is Tretton37 task, developed using Scala.

After running the application, a new folder named ```website``` would appear in the project Directory, which contains downloaded files of Tretton37 website.

The result assumption is the structure that Google Chrome provides in inspection mode, and ```sources``` tab.

![alt text](./GoogleChromeInspectionSource.png)


## Getting started - Install, Build and Run

To use this application, you need ```SBT 1.3.2``` and ```Java 8```.

With below commands, you can run tests and the application.

```
sbt clean test
sbt clean run
```

## Design of Code
The application is based on two major classes, ```JsoupUtils``` and ```Downloader```.

First, application gets directories and files information, such as name and web address.

Then categorises the addresses level by level, for recursive downloading.

Until this section, any possible function works in ```Future Context```, which run in separate thread, managed by ```Execution Context```.

In downloading stage, all elements in same level are independent, so they can be downloaded in separate thread at the same time.

For example if there are three elements in first level, they will be downloaded simultaneously.

But for downloading each level, we need the previous level be downloaded, so it waits for previous level downloading.


In developing this code, one important factor was to rely on functional programming features and high order functions and avoid using ```side effects```.

## List of Stories

- [ ] Create tests for TDD purpose
- [ ] Parse and store documents names and addresses
- [ ] Write download functions for File and Folder Creating
- [ ] Write download functions for simultaneous downloading
- [ ] Use all functions to run the application

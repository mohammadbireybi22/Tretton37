import Dependencies._

ThisBuild / scalaVersion     := "2.13.8"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "Tretton37",
    libraryDependencies += scalaTest % Test
  )

libraryDependencies ++= Seq(
  "org.jsoup" % "jsoup" % "1.8.3"
)



object InputsOutputs {

  val rawAddressList = List(
    "/humans.txt",
    "/assets/i/join.jpg",
    "/assets/i/contact.jpg",
    "/assets/i/covid-19.jpg",
    "/assets/i/events.jpg",
    "/assets/i/blog.jpg",
    "/assets/i/knowledge-sharing.jpg",
    "/assets/i/meet.jpg",
    "/assets/i/privacy-policy.jpg",
    "/assets/i/what-we-do.jpg",
    "/assets/i/who-we-are.jpg",
    "/assets/css/main.css?f4bb883e-637a-4986-ac20-23961a79f06d",
    "/assets/i/favicon.png",
    "/assets/i/delivery_teams.svg",
    "/assets/i/software_delivery.svg",
    "/assets/i/services.svg",
    "/assets/i/_tretton37_slogan_white.svg",
    "/assets/i/_tretton37_slogan_white.svg",
    "/assets/js/video.js",
    "/assets/js/index.js",
    "/assets/i/logos/alfa_laval.svg",
    "/assets/i/logos/dfds.svg",
    "/assets/i/logos/arriva.svg",
    "/assets/i/logos/cdon.svg",
    "/assets/i/logos/db.svg",
    "/assets/i/logos/diaverum.svg",
    "/assets/i/logos/elon.svg",
    "/assets/i/logos/ica.svg",
    "/assets/i/logos/modity.svg",
    "/assets/i/logos/paradox.svg",
    "/assets/i/logos/silverrail.svg",
    "/assets/i/logos/verisure.svg",
    "/assets/js/lib/polyfills.js",
    "/assets/js/lib/common.js?a9bb44a2-a8b2-439e-b297-14f19f5d0424"
  )

  val addressParseLevelZeroResponse = Map("humans.txt" -> "", "assets" -> "")
  val addressParseLevelOneResponse = Map("css" -> "/assets", "i" -> "/assets", "js" -> "/assets")
  val addressParseLevelTwoResponse = Map(
    "blog.jpg" -> "/assets/i",
    "privacy-policy.jpg" -> "/assets/i",
    "join.jpg" -> "/assets/i",
    "events.jpg" -> "/assets/i",
    "covid-19.jpg" -> "/assets/i",
    "main.css?f4bb883e-637a-4986-ac20-23961a79f06d" -> "/assets/css",
    "logos" -> "/assets/i",
    "favicon.png" -> "/assets/i",
    "index.js" -> "/assets/js",
    "who-we-are.jpg" -> "/assets/i",
    "video.js" -> "/assets/js",
    "knowledge-sharing.jpg" -> "/assets/i",
    "meet.jpg" -> "/assets/i",
    "delivery_teams.svg" -> "/assets/i",
    "contact.jpg" -> "/assets/i",
    "services.svg" -> "/assets/i",
    "_tretton37_slogan_white.svg" -> "/assets/i",
    "software_delivery.svg" -> "/assets/i",
    "lib" -> "/assets/js",
    "what-we-do.jpg" -> "/assets/i"

  )

  val addressParseLevelThreeResponse = Map(
    "diaverum.svg" -> "/assets/i/logos",
    "alfa_laval.svg" -> "/assets/i/logos",
    "polyfills.js" -> "/assets/js/lib",
    "db.svg" -> "/assets/i/logos",
    "silverrail.svg" -> "/assets/i/logos",
    "verisure.svg" -> "/assets/i/logos",
    "arriva.svg" -> "/assets/i/logos",
    "paradox.svg" -> "/assets/i/logos",
    "cdon.svg" -> "/assets/i/logos",
    "ica.svg" -> "/assets/i/logos",
    "common.js?a9bb44a2-a8b2-439e-b297-14f19f5d0424" -> "/assets/js/lib",
    "modity.svg" -> "/assets/i/logos",
    "dfds.svg" -> "/assets/i/logos",
    "elon.svg" -> "/assets/i/logos"
  )

  val addressParseLevelFourResponse = Map.empty
}

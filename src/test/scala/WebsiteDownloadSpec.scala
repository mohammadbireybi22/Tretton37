import Downloader.DownloaderImplementation
import InputsOutputs._
import JsoupUtils.JsoupUtilsImplementation
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures.whenReady

class WebsiteDownloadSpec extends FlatSpec with Matchers {

  val downloader = new DownloaderImplementation(new JsoupUtilsImplementation)

  "The Downloader object" should "do tests" in {

    whenReady(downloader.determineDocumentsAddress(rawAddressList, 0)){ result =>
      result shouldEqual addressParseLevelZeroResponse
    }

    whenReady(downloader.determineDocumentsAddress(rawAddressList, 1)){ result =>
      result shouldEqual addressParseLevelOneResponse
    }

    whenReady(downloader.determineDocumentsAddress(rawAddressList, 2)){ result =>
      result shouldEqual addressParseLevelTwoResponse
    }

    whenReady(downloader.determineDocumentsAddress(rawAddressList, 3)){ result =>
      result shouldEqual addressParseLevelThreeResponse
    }

    whenReady(downloader.determineDocumentsAddress(rawAddressList, 4)){ result =>
      result shouldEqual addressParseLevelFourResponse
    }
  }
}

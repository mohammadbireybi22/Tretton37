import Downloader.{DownloaderAlgebra, DownloaderImplementation}
import JsoupUtils.{JsoupUtilsAlgebra, JsoupUtilsImplementation}
import Tretton37Downloader.downloader.{createRootDocuments, getDocumentsInfo, saveDocuments}
import Tretton37Downloader.jsoupUtils.getWebsiteDocument

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

object Tretton37Downloader extends App {
  val websiteURL = "https://tretton37.com"

  val jsoupUtils: JsoupUtilsAlgebra = new JsoupUtilsImplementation
  val downloader: DownloaderAlgebra = new DownloaderImplementation(jsoupUtils)

  val websiteDocument = getWebsiteDocument(websiteURL)
  createRootDocuments(websiteDocument)

  val downloaded = for {
    websiteBaseData <- getDocumentsInfo(websiteDocument)
    result <- saveDocuments(websiteURL, websiteBaseData)
  } yield {
    result
  }

  Await.result(downloaded, Duration.Inf)
}

package JsoupUtils

import org.jsoup.Jsoup
import org.jsoup.nodes.{Document, Element}

import scala.collection.convert.ImplicitConversions.`list asScalaBuffer`
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class JsoupUtilsImplementation extends JsoupUtilsAlgebra {

  private lazy val timeout = 10000
  private lazy val link = "link"
  private lazy val href = "href"
  private lazy val image = "img"
  private lazy val source = "src"
  private lazy val script = "script"

  def getWebsiteDocument(websiteURL: String): Document = {
    Jsoup.connect(websiteURL).timeout(timeout).get
  }

  def extractElementsInfo(htmlElement: Element, htmlIdentifiers: (String, String)): Future[List[String]] = {

    Future {
      htmlIdentifiers match {
        case (tagName, addressTag) =>
          htmlElement.getElementsByTag(tagName)
            .map(_.attr(addressTag)).toList
      }
    }
  }

  def getWebsiteFileStructureData(htmlElement: Element): Future[List[String]] = {

    for {
      linkData <- extractElementsInfo(htmlElement, (link, href))
      imagesData <- extractElementsInfo(htmlElement, (image, source))
      scriptsData <- extractElementsInfo(htmlElement, (script, source))
    } yield {
      linkData concat imagesData concat scriptsData
    }
  }
}

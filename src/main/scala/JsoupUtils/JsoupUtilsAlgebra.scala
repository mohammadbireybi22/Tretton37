package JsoupUtils

import org.jsoup.nodes.{Document, Element}

import scala.concurrent.Future

trait JsoupUtilsAlgebra {

  def getWebsiteDocument(websiteURL: String): Document

  def extractElementsInfo(htmlElement: Element, htmlIdentifiers: (String, String)): Future[List[String]]

  def getWebsiteFileStructureData(htmlElement: Element): Future[List[String]]
}

package Downloader

import JsoupUtils.JsoupUtilsAlgebra
import org.jsoup.nodes.{Document, Element}

import sys.process._
import java.io.{BufferedWriter, File, FileWriter}
import java.net.URL
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.language.postfixOps

class DownloaderImplementation(jsoupUtilsAlgebra: JsoupUtilsAlgebra) extends DownloaderAlgebra {

  private lazy val http = "http"
  private lazy val cdn = "cdn"
  private lazy val slashString = "/"
  private lazy val slashCharacter = '/'
  private lazy val rootDirectoryAddress = "./website"
  private lazy val rootFileAddress = "./website/index.html"
  private lazy val fileIdentifier = "."

  def normalizeWebsiteBaseData(websiteElement: Element): Future[List[String]] = {
    for {
      websiteFileStructureData <- jsoupUtilsAlgebra.getWebsiteFileStructureData(websiteElement)
    } yield {

      websiteFileStructureData.filter { address =>
        !address.startsWith(http) &&
          !address.contains(cdn) &&
          address.nonEmpty
      }
        .map { address =>
          if (!address.startsWith(slashString))
            slashString + address
          else
            address
        }
        .sortWith(
          _.count(_ == slashCharacter) < _.count(_ == slashCharacter)
        )
    }
  }

  def determineDocumentsAddress(addresses: List[String], level: Int): Future[Map[String, String]] = {

    Future {
      addresses
        .filter { address =>
          address.count(_ == slashCharacter) > level
        }
        .groupBy { address =>
          address.split(slashString)(level + 1)
        }
        .map {
          case (documentName, address) =>
            (
              documentName,
              address
                .map(_.split(slashString)
                  .slice(0, level + 1)
                  .mkString(slashString)).head
            )
        }
    }
  }

  def getDocumentsInfo(element: Element): Future[List[Map[String, String]]] = {
    for {
      normalizedWebsiteBaseData <- normalizeWebsiteBaseData(element)
      depth = normalizedWebsiteBaseData.map(_.count(_ == slashCharacter)).max

      websiteBaseData <- Future.sequence(
        List.fill(depth)(normalizedWebsiteBaseData).zipWithIndex.map {
          case (value, index) =>
            determineDocumentsAddress(value, index)
        }
      )
    } yield {
      //      websiteBaseData.foreach(println)
      websiteBaseData
    }
  }

  def createRootDocuments(rootFileData: Document): Unit = {

    new File(rootDirectoryAddress).mkdir
    val indexFile = new File(rootFileAddress)
    val fileWriter = new FileWriter(indexFile.getAbsoluteFile)
    val bufferWriter = new BufferedWriter(fileWriter)
    bufferWriter.write(rootFileData.html)
    bufferWriter.close()
  }

  def saveDocuments(websiteURL: String, data: List[Map[String, String]]): Future[List[Any]] = {

    Future.sequence(
      data.map { documentInfo =>
        documentInfo.map {
          case (document, address) =>
            Future {
              if (document.contains(fileIdentifier)) {
                println(Console.YELLOW + "Downloading File: " + document + Console.WHITE)
                new URL(websiteURL + address + slashString + document) #> new File(rootDirectoryAddress + address + slashString + document) !!
              } else {
                println(Console.GREEN + "Creating Folder: " + document + Console.WHITE)
                new File(rootDirectoryAddress + address + slashString + document).mkdir
              }
            }
        }
      }.flatMap(_.toList)
    )
  }
}

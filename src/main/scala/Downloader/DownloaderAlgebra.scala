package Downloader

import org.jsoup.nodes.{Document, Element}

import scala.concurrent.Future

trait DownloaderAlgebra {

  def normalizeWebsiteBaseData(websiteElement: Element): Future[List[String]]

  def determineDocumentsAddress(addresses: List[String], level: Int): Future[Map[String, String]]

  def getDocumentsInfo(websiteElement: Element): Future[List[Map[String, String]]]

  def createRootDocuments(rootFileData: Document): Unit

  def saveDocuments(websiteURL: String, data: List[Map[String, String]]): Future[List[Any]]
}
